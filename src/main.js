// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import vuex from 'vuex'
import store from './store/store'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '../static/style/normalize.css'
import '../static/style/self_element.css'
import * as filters from './filters/filter'

import apiLib from '../static/script/lib/lib.js'
import rules from '../static/script/lib/rules'
Vue.prototype.$sysUrl = apiLib._GetQueryString('url')
//alert(apiLib._GetQueryString('url'))
Vue.prototype.$axios = apiLib
Vue.prototype.$rules = rules
Vue.config.productionTip = false

Vue.use(router)
Vue.use(vuex)
Vue.use(ElementUI)
Vue.use(ElementUI,{ size: 'mini' })

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {  // 需要权限,进一步进行判断
    if (store.state.token||window.sessionStorage.token=="1") {  // 通过vuex state获取当前的token是否存在
      next();
    }
    else {    //如果没有权限,重定向到登录页,进行登录
      next({
        path: '/login',
        query: {redirect: to.fullPath}  // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
    }
  }
  else { //不需要权限 直接跳转
    next();
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})

Object.keys(filters).forEach(key => {
  //console.log(key)
  Vue.filter(key, filters[key])
})
