import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  //mode: 'history',
  routes: [
    {
      meta: {
        requireAuth: true
      },
      path: '/',
      name: 'Index',
      component: resolve => require(['@/components/index.vue'],resolve),
      children: [
        {
          meta: {
            requireAuth: true
          },
          path: '/account/userIndex',//用户管理列表
          component: resolve => require(['@/components/roleMange/userManage/userList.vue'],resolve),
          props:true,
          hidden:true,
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/userEdit',//用户编辑
          component: resolve => require(['@/components/roleMange/userManage/userEdit.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/roleIndex',//角色管理列表
          component: resolve => require(['@/components/roleMange/roles/roleList.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/roleEdit',//角色编辑
          component: resolve => require(['@/components/roleMange/roles/roleEdit.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/menuIndex',//菜单管理列表
          component: resolve => require(['@/components/roleMange/orders/orderList.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/menuEdit',//菜单编辑
          component: resolve => require(['@/components/roleMange/orders/orderEdit.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/deptIndex',//机构管理列表
          component: resolve => require(['@/components/roleMange/organs/organList.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/deptEdit',//机构编辑
          component: resolve => require(['@/components/roleMange/organs/organEdit.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/dicIndex',//字典管理列表
          component: resolve => require(['@/components/roleMange/dictionaries/dicList.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/dicEdit',//字典编辑
          component: resolve => require(['@/components/roleMange/dictionaries/dicEdit.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/sysIndex',//系统管理列表
          component: resolve => require(['@/components/roleMange/system/sysList.vue'],resolve),
          props:true
        },
        {
          meta: {
            requireAuth: true
          },
          path: '/account/sysEdit',//系统编辑
          component: resolve => require(['@/components/roleMange/system/sysEdit.vue'],resolve),
          props:true
        },
      ]
    },
    {
      path:"/login",
      name:"login",
      component: resolve => require(['@/components/login.vue'],resolve),
    },
  ]
})
